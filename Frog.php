<?php

require_once('Animal.php');

class Frog extends Animal
{
    public $legs = 4;
    public $cold_blooded = "True";
    public function jump()
    {
        echo "Hop hop";
    }
}
