<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP</title>
</head>

<body>
    <?php
    require_once('Animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    echo "<h2>Hewan Pertama</h2>";
    $sheep = new Animal("Shaun");
    echo "Nama Hewan : $sheep->name <br>";
    echo "Jumlah Kaki : $sheep->legs <br>";
    echo "Berdarah Dingin  : $sheep->cold_blooded <br>";


    echo "<h2>Hewan Kedua</h2>";
    $sungokong = new Ape("Kera Sakti");
    echo "Nama Hewan : $sungokong->name <br>";
    echo "Jumlah Kaki : $sungokong->legs <br>";
    echo "Berdarah Dingin  : $sungokong->cold_blooded <br>";
    echo "Suara : ";
    $sungokong->yell();


    echo "<h2>Hewan Ketiga</h2>";
    $kodok = new Frog("Buduk");
    echo "Nama Hewan : $kodok->name <br>";
    echo "Jumlah Kaki : $kodok->legs <br>";
    echo "Berdarah Dingin  : $kodok->cold_blooded <br>";
    echo "Jalan :";
    $kodok->jump();

    ?>
</body>

</html>